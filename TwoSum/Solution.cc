#include "Solution.h"

#include <unordered_map>

std::vector<int> Solution::twoSum(const std::vector<int>& nums, int target) {
  std::vector<int> ret(2);
  std::unordered_map<int, int> possibleSolutions;
  for (int i = 0; i < nums.size(); i++) {
    if (possibleSolutions.find(target - nums[i]) == possibleSolutions.end()) {
      possibleSolutions[nums[i]] = i;
    } else {
      ret[0] = possibleSolutions[target - nums[i]];
      ret[1] = i;
      return ret;
    }
  }
  return ret;
}
