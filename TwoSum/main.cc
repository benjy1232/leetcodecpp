#include <iostream>
#include "Solution.h"

int main(int argc, char** argv) {
  std::vector<int> nums {2,7,11,15};
  int target = 9;
  Solution soln;
  std::vector<int> sol = soln.twoSum(nums, target);
  for (int num: sol) {
    std::cout << num << std::endl;
  }
}
