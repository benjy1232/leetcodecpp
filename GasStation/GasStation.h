#pragma once
#include <vector>

class Solution {
 public:
  int canCompleteCircuit(const std::vector<int>&, std::vector<int>&);
};
