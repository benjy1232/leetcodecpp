#include "GasStation.h"

int Solution::canCompleteCircuit(const std::vector<int>& gas,
                                 std::vector<int>& cost) {
  // gas is the amount of gas that is refilled at the station;
  // cost is the amount of gas needed to get from the previous station to the
  // next station return -1 if it cannot be completed
  int n = gas.size();
  for (int startingGasStation = 0; startingGasStation < n; startingGasStation++) {
    int totalGasLeft = gas[startingGasStation];
    int currentGasStation = startingGasStation;
    int nextGasStation = (startingGasStation == n - 1) ? 0 : startingGasStation + 1;

    while (nextGasStation != startingGasStation) {
      totalGasLeft = totalGasLeft - cost[currentGasStation] + gas[nextGasStation];
      nextGasStation = (nextGasStation == n - 1) ? 0 : nextGasStation + 1;
    }
  }
  return -1;
}
