#include <iostream>
#include "GasStation.h"

int main(void) {
  std::vector<int> gas{1, 2, 3, 4, 5};
  std::vector<int> cost{3, 4, 5, 1, 2};
  Solution soln;
  std::cout << soln.canCompleteCircuit(gas, cost) << std::endl;
}
