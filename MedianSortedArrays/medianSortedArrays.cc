#include "medianSortedArrays.h"

double Solution::findMedianSortedArrays(const std::vector<int>& nums1,
                              std::vector<int>& nums2) {
  // Finds median of sorted array
  int currentMedian = 0;
  int currentIdx = 0;
  int finalArraySize = nums1.size() + nums2.size();
  const int midPoint = finalArraySize / 2 + 1;

  std::vector<int>::const_iterator it1 = nums1.begin();
  std::vector<int>::iterator it2 = nums2.begin();

  if (finalArraySize % 2 == 0) {
    return findMedianEvenSortedArrays(nums1, nums2);
  }

  while (currentIdx < midPoint) {
    if (it1 == nums1.end()) {
      break;
    }

    if (it2 == nums2.end()) {
      break;
    }

    if (*it1 > *it2) {
      currentMedian = *it2;
      *it2++;
    } else {
      currentMedian = *it1;
      *it1++;
    }
    currentIdx++;
  }

  if (currentIdx >= midPoint) {
    return currentMedian;
  }

  while (it1 != nums1.end() && currentIdx < midPoint) {
    currentMedian = *it1;
    currentIdx++;
    it1++;
  }

  while (currentIdx < midPoint) {
    currentMedian = *it2;
    currentIdx++;
    it2++;
  }
  return currentMedian;
}

double Solution::findMedianEvenSortedArrays(const std::vector<int>& nums1,
                                  std::vector<int>& nums2) {
  int lowerValue;
  int upperValue = 0;
  int currentIdx = 0;
  int midPoint = (nums1.size() + nums2.size()) / 2 + 1;
  std::vector<int>::const_iterator it1 = nums1.begin();
  std::vector<int>::const_iterator it2 = nums2.begin();
  while (currentIdx < midPoint) {
    if (it1 == nums1.end()) {
      break;
    }

    if (it2 == nums2.end()) {
      break;
    }

    lowerValue = upperValue;
    if (*it1 > *it2) {
      upperValue = *it2;
      *it2++;
    } else {
      upperValue = *it1;
      *it1++;
    }
    currentIdx++;
  }

  if (currentIdx >= midPoint) {
    return ((double)upperValue + (double)lowerValue) / 2;
  }

  while (it1 != nums1.end() && currentIdx < midPoint) {
    lowerValue = upperValue;
    upperValue = *it1;
    currentIdx++;
    it1++;
  }

  while (currentIdx < midPoint) {
    lowerValue = upperValue;
    upperValue = *it2;
    currentIdx++;
    it2++;
  }
  return ((double)upperValue + (double)lowerValue) / 2;
}

