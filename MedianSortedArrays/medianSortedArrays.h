#pragma once
#include <vector>

class Solution {
    public:
        double findMedianSortedArrays(const std::vector<int>&, std::vector<int>&);
        double findMedianEvenSortedArrays(const std::vector<int>&, std::vector<int>&);
};
