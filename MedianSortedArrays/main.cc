#include <iostream>
#include "medianSortedArrays.h"

int main(void) {
  std::vector<int> list1{1, 3};
  std::vector<int> list2{2};
  Solution soln;
  std::cout << soln.findMedianSortedArrays(list1, list2) << std::endl;
  list2.push_back(4);
  std::cout << soln.findMedianSortedArrays(list1, list2) << std::endl;
}
